package funkcjonalnosc;

import java.util.Scanner;

public class Kalkulator {
	
	/* 
	 * Modyfiaktory widoczności w klasach służą do zabezpieczenia i możliwości zarządzania widocznością pól i metod
	 * (czyli składowych) naszej tworzonej klasy. Dostępne mamy następujące możliwości zakresu widoczności:
	 * 
	 * - private - widoczność elementu jest ograniczona tylko do obecnie tworzonej klasy. Oznacza to, że zarówno
	 * pola jak i metody klasy będą mogły z niej korzystać, zaś inne klasy i inne obiekty/metody/pola nie będą 
	 * miały do nich dostępu
	 * 
	 * - public - składowe są widoczne wszędzie i dla każdego. Wystarczy utworzyć obiekt lub użyć słowa static 
	 * 
	 * - protected - składowe z tym modifikatorek widoczne są dla wszystkich klas będą w danej paczce ora dla
	 * klas, które dziedziczą z tworzonej przez nas klasy
	 * 
	 * - internal (domyślne) - pola są widoczne i zachowują się jak publiczne ale tylko dla klas, które są w tej samej
	 * paczce kodu (module).
	 */
	
	/*
	 * Poniższa zmienna to zmienna tablicowa dwuwymiarowa (tzw. 2D). Języki programowania na dzień dzisiejszy
	 * pozwalają na tworzenie także wielowymiarowych tablic. 
	 * 
	 * Tego typu zmienna przypomina swoją konstrukcją arkusz kalkulacyjny. Możliwe jest zapisywanie wartości w wierszach
	 * (kolejne wartości w pierwszym nawiasie - tym zaraz po nazwie zmiennej) oraz zapisywanie w "kolumnach" zmiennej
	 * (rozmiar określa drugi nawias). 
	 * 
	 * Oczywiście bardziej poprawnie (lecz być może mniej przejrzyście) byłoby stwierdzić, że zmienna ta posiada 
	 * wartość tablicową do przechowywania zminennych tablicowych (dowolnych rozmiarów)
	 */
	
	private double zmienne[][];
	
	private int indeks;
	
	private Scanner skan;
	
	private String znaki[] = {" + ", " - ", " * "};
	
	public void menu() {
		int opcja=0;
		String opcje="\n\nWybierz jedną z opcji:\n1.Dodawanie\n2.Odejmowanie\n3.Mnożenie\n"+
				"4.Dzielenie\n5.Reszta z dzielenia\n6.Potęgowanie\n7.Pierwiastkowanie\n8.Wypisz zapisane wyniki"
				+ "\n9.Wyzeruj pamięć\nDOWOLNA LICZBA - POWRÓT DO PROGRAMU\n"+
							"Twój wybór: ";
		
		//Scanner skan = new Scanner(System.in);
		for(;;) {
			System.out.print(opcje);
			try {
				opcja=Integer.valueOf(skan.nextLine());				
			}
			catch (Exception ex) {
				System.err.println("Podałeś złą liczbę! Spróbuj jeszcze raz! (Wciśniej cokolwiek by kontynuować...)");
				skan.nextLine();
				continue;
			}
		
			switch(opcja) {
				case 1: 
				case 2:
				case 3: 
				case 4: 
				case 5:
				case 6: 
				case 7: wprowadzDzialanie(opcja-1);break;
				case 8: wypiszZmienne(); break;
				case 9: zerujZmienne(); break;
				default: return;
			}
		}
	}
	
	private void wypiszZmienne() {
		for(double[] zm : zmienne) {
			System.out.println("Zapisane zmienne w kolejnym indeksie: ");
			for(double z : zm) {
				System.out.print(z + " ");
			}
			System.out.print('\n');
		}
		
	}
	
	private void zerujZmienne() {
		zmienne=null;		
	}
	
	private String wypiszDzialanie(int opcja) {
		String wyjscie ="";
//		if (zmienne[this.indeks].length==0|| zmienne[this.indeks]==null) return "0";
//		if (zmienne[this.indeks].length<2) return String.valueOf(zmienne[this.indeks][0]);
		for (double zm : zmienne[this.indeks]) {
			wyjscie+= zm + znaki[opcja];
		}
		return wyjscie.substring(0, wyjscie.length() - 3);
	}
	
	private void wypiszTekst(String s, double wynik, int opcja) {
		System.out.println(s + " " + wypiszDzialanie(opcja) + " = " + wynik);
	}
	
	private void wprowadzDzialanie(int opcja) {
		System.out.print("Wprowadź wartości, które następnie będą brały udział a liczeniu: ");
		//Scanner skan = new Scanner(System.in);
		//String ciag = skan.nextLine();
		znajdzWartosci(skan.nextLine()); 
		double wynik = 0;
		switch (opcja) {
			case 0: wynik=dodawanie(); break;
			case 1: wynik=odejmowanie(); break;
			case 2: wynik=mnozenie(); break;
		}
		wypiszTekst("Wynik działania:", wynik, opcja); 
		dodajDoTablicy(wynik);
		dodajDoTablicy(opcja);
		this.indeks++;
	}
	
	private void znajdzWartosci(String tekst) {
		if ((tekst.isEmpty() || tekst.isBlank()) && zmienne == null) {
			dodajDoTablicy(0);
			return;
		}
		String tmp ="";
		tekst+=" ";
		boolean czyZmiennoprzecinkowa = false;
		boolean czyZmienna= false;
		for (char znak : tekst.toCharArray()) {

			//12.9
			if (znak >= '0' && znak <= '9')
				tmp+=znak;
			else if (czyZmiennoprzecinkowa) {
				//ten fragment wykona się gdy użytkownik wcześniej wpisał kropkę lub przecinek, jednak
				//w kolejnym znaku nie przekazał już żadnej cyfry. Oznacza to, że tutaj należy dodać obecnie zgromadzone
				//cryfry jako liczbę do naszej tabeli
				//najpierw cofamy o jeden znak wprowadzone dane w zmiennej tmp
				if (!(tmp.charAt(tmp.length()-1) >= '0' && tmp.charAt(tmp.length()-1) <= '9')) 
					tmp=tmp.substring(0,tmp.length()-1);
				//dodajemy do tablicy liczbę (zamieniając ewentualnie przecinki na kropki -> przecinek może spowodować błąd konwersji)
				dodajDoTablicy(tmp.replace(',', '.'));
				tmp="";
//				//nie mamy jednak do czycnienia ze zmiennoprzecinkową(float) więc zmieniamy wartość na false
//				czyZmiennoprzecinkowa=false;
			}
			else if (czyZmienna) {
				
			}
			else if ((znak == '+' || znak=='-') && tmp.isEmpty()) 
				tmp+=znak;
			//jeżeli ktoś już dodał znak i pojawia się kolejny i kolejny to ten fragment kodu będzie pilnował, żeby
			//wczytać ostatni podany przez cyfrą znak
			else if ((znak == '+' || znak=='-') && 
					(tmp.charAt(tmp.length()-1) == '+' || tmp.charAt(tmp.length()-1) == '-'))
				tmp=tmp.substring(0, tmp.length()-1)+znak;
			else if (znak == '$' && tmp.isEmpty()) {
				czyZmienna=true;
				tmp+=znak;
			}
			else if ((znak=='.' || znak==',') && !czyZmienna) {
				tmp+=znak;
				czyZmiennoprzecinkowa=true;
				//continue;
			}
			else {
				dodajDoTablicy(tmp.replace(',', '.'));
				tmp="";
			}	
		}			 
	}
	
	private void dodajDoTablicy(String wartosc) {
		dodajDoTablicy(wartosc, false);
	}
	
	private void dodajDoTablicy(String wartosc, boolean zmienna) {
		int lokalIndeks = 0;
		try {
			//użyty substring ponieważ pierwszym znakiem w tablicy znaków będzie $ (bo on oznacza zmienną)
			lokalIndeks=Integer.valueOf(wartosc.substring(1));
			//niezbyt efektywne ale na pewno pokazuje możliwość wykorzystania trójstanowego operatora
			//operator ten pozwala na zadanie pytania (jak if tylko bez słowa kluczowego) po czym może przyjąć jeden z dwóch
			//stanów:
			// po znaku zapytania (?) - kod wykonuję się gdy wartość warunku równa jest true
			// po znaku dwukropka (:) - kod ywklonuję się gdy wartość warunkó to false
			//Operator trójstanowy można wykorzystywać jako wstawki w kodzie (nie trzeba go używać niezależnie)
			dodajDoTablicy( (zmienna) ? zmienne[lokalIndeks][zmienne[lokalIndeks].length - 2] : Double.valueOf(wartosc));
			
//			if (zmienna)
//				dodajDoTablicy(zmienne[lokalIndeks][zmienne[lokalIndeks].length - 2]);
//			else
//				dodajDoTablicy(Double.valueOf(wartosc));
			
		}
		catch(Exception ex) {
			return;
		}	
		
	}	
	
	private void dodajDoTablicy(double wartosc) {
		//sprawdzamy czy nasza tablica 2D zawiera jakiekolwiek wartości na obecnie przetwarzanym indeksie
		//tj. czy posiada jakiekolwiek wartości; jeżeli nie posiada - tworzymy jej pierwszy "wiersz" (pierwszy
		//zestaw wartości)
		if (zmienne == null) 
			zmienne = new double[this.indeks+1][0];
		//jeżeli posiadamy już jakieś wartości zapisane w tablicy zmienne lecz nie posiadamy odpowiedniej jej wielkości
		//(za mało zestawów wartości) to tworzymy zmienną tymczasową (w tym wypadku zm2d) i kopiujemy 
		//zestaw po zestawie do zmiennej tablicowej tymczasowej
		//po czym przypisujemy go do zmienne (teraz zmienne będą zawierać dotychczasowe wyniki oraz jeden pusty zestaw
		else if (zmienne.length-1 < this.indeks){
			double zm2d[][] = new double[this.indeks+1][];
			for (int i=0;i<zmienne.length;i++) {
				zm2d[i]=zmienne[i];
			}
			zmienne=zm2d;
		}
		
		int indeks = 0;
		if (zmienne[this.indeks] != null) {
			indeks = zmienne[this.indeks].length;
		}
		double tmp[]=new double[indeks+1];
		//tmp=zmienne; => błąd, przypisanie zmiennej z jej wartościami do nowej zmiennej (niejawne utworzenie NOWEJ zmiennej), nie zaś przepisanie wartości z poprzedniej zmiennej do nowej
		
		for(int i=0;i<indeks;i++) {
			tmp[i]=zmienne[this.indeks][i];
		}
		tmp[indeks]=wartosc;
		zmienne[this.indeks]=tmp;
	}
	
	private double dodawanie() {
		double wynik = 0;		
		for(double zm : zmienne[this.indeks]) {				
			wynik+=zm;
		}
		return wynik;
		//System.out.println("\n\n" + liczby + " = " + a);
	}
	
	private double odejmowanie() {		
		double wynik = zmienne[this.indeks][0];
		for(int i=1;i<zmienne[this.indeks].length;i++) {				
			wynik-=zmienne[this.indeks][i];
		}
		return wynik;
		
	}
	
	private double mnozenie() {		
		double wynik = zmienne[this.indeks][0];
		for(int i=1;i<zmienne[this.indeks].length;i++) {				
			wynik*=zmienne[this.indeks][i];
		}
		return wynik;
	}
	
	private void modulo() {
		
	}
	
	/*
	 * Poniże został utworzony konstruktor klasy. Konstruktor do specjalny rodzaj funkcji, która pozwala nam na ustawienie
	 * wartości poszczególnych składowych naszej klasy na odpowiednie wartości, a także na reakcję w przypadku 
	 * na różne wartości zmiennych obecnych już w programie. 
	 * 
	 * Każda klasa posiada konstruktor domyślny, który tworzony jest NIEJAWNIE (niezależnie od działań programisty). Konstruktor
	 * domyślny przeważnie nic nie robi. My za to, jako programiści, możemy definiować własny konstruktor/konstruktory.
	 * Zdefiniowanie jakiegokolwiek konstruktora powoduje, że konstruktor domyślny nie jest jest już tworzony. 
	 */
	
	public Kalkulator(Scanner skan) {
		//System.out.println("WŁAŚNIE KONSTRUUJĘ I BUDZĘ DO ŻYCIA KLASĘ! BĘDZIE OBIEKT!");
		this.skan=skan;
		this.indeks=0;
		//menu();
		
	}
	
	
}
