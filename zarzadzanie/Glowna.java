package zarzadzanie;
import funkcjonalnosc.*;

public class Glowna {
	
	//to jest przyk�ad na odwo�anie si� do biblioteki Java gdy nie u�yjemy klauzuli import; w tym wypadku musimy poda� pe�n� �cie�k� do biblioteki (co jak wida� dzia�a)
	private static java.util.Scanner wejscie;
	
	public void powitanie() {
		System.out.println("Zaczynamy obiektow��!");
	}
	
	public int wylosujNumerek(int minimum) {
		int numer = minimum;
		return numer;
	}
	
	private static boolean menu() {
		try {
			System.out.print("Witaj w programie-pomocy matematycznej. Wybierz rodzaj pomocy:\n"+
				"1.Kalkulator\n2.Geometria i figury\n3.Obliczanie dziala�\nDodowlna sekwencja znak�w - koniec programu."+
							"\n\nTw�j wyb�r: ");
			switch(Integer.valueOf(wejscie.nextLine())) {
				case 1: new Kalkulator(wejscie).menu(); break;
				case 2: new Obwody(); break;
				default: return false;
			}			
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		
		
		
		wejscie = new java.util.Scanner(System.in);
		for(;;) {
			if (!menu()) break;
		}
		wejscie.close();
		System.out.println("\n\nDo mi�ego zobaczenia");
		//Kalkulator kalk = new Kalkulator();
		
	}
}
